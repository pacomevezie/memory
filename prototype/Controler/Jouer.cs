﻿using prototype.Model;
using prototype.View;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace prototype.Controler
{
    public class Jouer
    {
        private static Carte[] deuxCartes = new Carte[2];
        private static DoublonCarte[] doublonDeCartes;
        private static Carte uneCarte = null;
        private static int nbreClic = 0;

        public static void SetDoublonsDeCartes(DoublonCarte[] lesDoublons)
        {

        }

        public static void Lancer()
        {
            Application.Current.MainWindow = new VueJouer();
            Application.Current.MainWindow.Show();
        }

        public static bool VerificationJeu()
        {
            return false;
        }

        public static void CliquerUneCarte()
        {
            uneCarte.SetStatus(false);
            uneCarte.AppareillerBouton();
        }

        public static bool JouerUnTour(Carte laCarte)
        {
            bool resultat = false;            
            uneCarte = laCarte;
            if(uneCarte.Getstatus() == false)
            {
                if(deuxCartes[0]== null)
                {
                    deuxCartes[0] = laCarte;
                    CliquerUneCarte();
                }
                if (deuxCartes[1] == null)
                {
                    deuxCartes[1] = laCarte;
                    CliquerUneCarte();
                    resultat = true;
                }

            }
            return resultat;
        }

        public static void VerifierUnTour()
        {
            if (uneCarte.GetDoublonCartes().GetStatus() == false)
            {
                InitialiserUnTourPerdu();
                MessageBox.Show("perdu");
            }
            else
            {
                MessageBox.Show("bien jouer");
            }
        }

        public static void MiseAZeroDesCartes()
        {
            uneCarte = null;
            deuxCartes = null;

        }

        public static void InitialiserUnTourPerdu()
        {
            deuxCartes[0].SetStatus(false);
            deuxCartes[0].AppareillerBouton();
            deuxCartes[1].SetStatus(false);
            deuxCartes[1].AppareillerBouton();
        }

        public static string Description()
        {
            return "oui";
        }
    }
}
