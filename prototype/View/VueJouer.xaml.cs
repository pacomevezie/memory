﻿using prototype.Controler;
using prototype.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace prototype.View
{
    /// <summary>
    /// Logique d'interaction pour VueJouer.xaml
    /// </summary>
    public partial class VueJouer : Window
    {

        //tableau des chemins pour accéder aux images
        private static string[] cheminLesImages;
        //Tableaux des images pour le jeu
        private string[] cheminLesImagesDuJeu;
        //Tableau des Cartes modèle
        private Carte[] lesCartesModels;
        //Tableau des cartes du jeu
        private Carte[] lesCartesJeu;
        //Nbre de Carte modèle
        private int nbreCarteModel = 2;
        //Nbre de carte généré
        private int nbreCarte = 4;
        //Nbre de Col / Rangés
        private int nbreColRang = 2;
        //Tableau de doublon de Carte
        private DoublonCarte[] lesDoublonsCartes;
        private bool rafraichir;
        private object leResultat;

        public VueJouer()
        {
            InitializeComponent();

            //Remplissage du tableau de toutes les images de l'application
            cheminLesImages = new string[19];
            for (int i = 0; i < 18; i++)
            {
                cheminLesImages[i] = "/Resources/image" + i + ".jpg";
            }
            //Mettre a disposition les images
            cheminLesImagesDuJeu = new string[nbreCarteModel + 1];
            if(cheminLesImages.Length != 0)
            {
                for(int i = 0; i < nbreCarteModel + 1; i++)
                {
                    cheminLesImagesDuJeu[i] = cheminLesImages[i];
                }
            }            
            //Créer des cartesModel
            lesCartesModels = new Carte[nbreCarteModel];
            for (int i = 0; i < nbreCarteModel; i++)
            {
                lesCartesModels[i] = new Carte(cheminLesImagesDuJeu[0], cheminLesImagesDuJeu[i + 1]);
            }
            //Mettre à jour les doublons
            lesDoublonsCartes = new DoublonCarte[nbreCarteModel];
            for (int i = 0; i < nbreCarteModel; i++)
            {
                lesDoublonsCartes[i] = lesCartesModels[i].GetDoublonCartes();
            }
            //et dans la classe jouer
            Jouer.SetDoublonsDeCartes(lesDoublonsCartes);

            //Générer les cartes du jeu
            lesCartesJeu = new Carte[nbreCarte];
            for (int i = 0; i < nbreCarteModel; i++)
            {
                lesCartesJeu[i] = lesCartesModels[i].GetDoublonCartes().Getcarte1();
                lesCartesJeu[i + nbreCarteModel] = lesCartesModels[i].GetDoublonCartes().GetCarte2();
            }
            //Mélanger carte 

            for (int i = 0; i < nbreCarteModel; i++)
            {
                GdFond.RowDefinitions.Add(new RowDefinition());
                GdFond.ColumnDefinitions.Add(new ColumnDefinition());
            }
            //Mélanger les cartes
            //lesCartesJeu.Service.LesServices(lesCartesJeu[] = lesCartesJeu);
            for(int i = 0; i < nbreCarte; i++)
            {
                GdFond.Children.Add(lesCartesJeu[i].Getbouton());
            }
            int k = 0;
            for (int i = 0; i < nbreColRang; i++)
            {
                for(int j = 0; j < nbreColRang; j++)
                {
                    Grid.SetRow(GdFond.Children[k], i);
                    Grid.SetColumn(GdFond.Children[k], j);
                    k++;
                }
            }
                                
        }
        public void BoutonClick(Object sender, RoutedEventArgs e)
        {
            MessageBox.Show("ok");
            bool result;
            foreach (Carte elem in lesCartesJeu)
            {
                if (sender.Equals(elem.Getbouton()))
                {
                    result = Jouer.JouerUnTour(elem);
                    SetRaffraichir(result);
                }
            }
        }

        private void SetRaffraichir(bool leResultat)
        {
            this.rafraichir = leResultat;
            OnPropertyChanged("rafraichir");
        }

        protected void OnPropertyChanged(string propertyName)
        {
            //throw new NotImplementedException();
            MessageBox.Show("OK" + rafraichir);
            if (rafraichir) Jouer.VerifierUnTour();
        }
    }
}
