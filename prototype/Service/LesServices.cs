﻿using prototype.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace prototype.Service
{
    public class LesServices
    {
        public static Image CreerUneImage(string leChemin)
        {
            Image rendu = new Image();
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            string chemin = leChemin;
            bi.UriSource = new Uri(chemin, UriKind.Relative);
            bi.EndInit();
            rendu.Stretch = Stretch.Fill;
            rendu.Source = bi;
            return rendu;
        }
        public static Carte[] MelangerLesCartes(Carte[] lesCartes)
        {
            Random random = new Random();
            return lesCartes.OrderBy(x => random.Next()).ToArray();
        }
    }
}
