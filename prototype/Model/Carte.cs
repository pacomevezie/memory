﻿using prototype.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace prototype.Model
{
    public class Carte
    {
        private static int nbreCarte = 0;
        private static int numero = 0;
        private DoublonCarte doublonCarte = null;
        private Button bouton = null;
        private bool status = false;
        private int valeur = 0;
        private string cheminImageDos = null;
        private string cheminImageFace = null;
        private Image imageDos = null;
        private Image imageFace = null;
        private string titre = null;
        private int numColFentre = 0;
        private int numRangFenetre = 0;
        private int identifiant = 0;

        public Carte(string leCheminImageDos, string leCheminImageFace)
        {
            nbreCarte++;
            this.valeur = nbreCarte;
            this.cheminImageDos = leCheminImageDos;
            this.cheminImageFace = leCheminImageFace;
            this.titre = "Carte modele n°" + nbreCarte;
            this.doublonCarte = new DoublonCarte(this);
        }
        public Carte(Carte laCarte)
        {
            numero++;
            this.identifiant = numero;
            this.imageDos = LesServices.CreerUneImage(laCarte.GetCheminImageDos());
            this.imageFace = LesServices.CreerUneImage(laCarte.GetCheminImageFace());
            this.valeur = laCarte.GetValeur();
            this.status = laCarte.Getstatus();
            this.bouton = new Button();
            this.AppareillerBouton();
        }
        public Button Getbouton()
        {
            return this.bouton;
        }
        public bool Getstatus()
        {
            return this.status;
        }
        public void SetStatus(bool leStatus)
        {
            this.status=leStatus;
        }
        public int GetValeur()
        {
            return this.valeur;
        }
        public DoublonCarte GetDoublonCartes()
        {
            return this.doublonCarte;
        }
        public void SetDoublonCartes(DoublonCarte leDoublon)
        {
            this.doublonCarte = leDoublon;
        }
        public int GetIdentifiant()
        {
            return this.identifiant;
        }
        public int GetNumColFenetre()
        {
            return this.numColFentre;
        }
        public void SetNumeColFenetre(int numCol)
        {
            this.numColFentre=numCol;
        }
        public int GetNumRangFenetre()
        {
            return this.numRangFenetre;
        }
        public void SetNumRangFenetre(int numRang)
        {
            this.numRangFenetre = numRang;
        }
        public string GetCheminImageDos()
        {
            return this.cheminImageDos;
        }
        public string GetCheminImageFace()
        {
            return this.cheminImageFace;
        }
        public void AppareillerBouton()
        {
            if (this.status == false)
            {
                this.bouton.Content = imageDos;
            }
            else
            {
                this.bouton.Content = imageFace;
            }
        }
        public string Description()
        {
            string resultat = "la valeur est : " + this.valeur + "\nle titre est : " + this.titre;
            resultat += "\nle chemin du dos est : " + this.cheminImageDos + "\nle chemin de la face est : ";
            resultat += this.cheminImageFace + "\nson statut est : " + this.status;
            return resultat;
        }
    }
}
